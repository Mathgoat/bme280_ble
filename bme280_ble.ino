#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

#define EnvironmentService BLEUUID((uint16_t)0x181A)
#define BatteryService BLEUUID((uint16_t)0x180F)


BLECharacteristic *tempCharacteristic = NULL;
BLECharacteristic *humidCharacteristic = NULL;
BLECharacteristic *pressureCharacteristic = NULL;
BLECharacteristic * batteryCharacteristic = NULL;

Adafruit_BME280 bme;
float temperature, humidity, pressure, battery;         
bool deviceConnected = false;


class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

void setup() {
  Serial.begin(115200);
  delay(100);
  bme.begin(0x76);
  
  // Create BLE Server
  BLEDevice::init("6TRON Bureau");
  BLEServer *server = BLEDevice::createServer();
  server->setCallbacks(new MyServerCallbacks());
  
  // Create environment service
  BLEService *eService = server->createService(EnvironmentService);
  BLEService *bService = server->createService(BatteryService);
  
  // Create environment characteristics
  tempCharacteristic = eService->createCharacteristic(BLEUUID((uint16_t)0x2A6E),BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
  humidCharacteristic = eService->createCharacteristic(BLEUUID((uint16_t)0x2A6F),BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
  pressureCharacteristic = eService->createCharacteristic(BLEUUID((uint16_t)0x2A6D),BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
  eService->addCharacteristic(tempCharacteristic);
  eService->addCharacteristic(humidCharacteristic);
  eService->addCharacteristic(pressureCharacteristic);
  tempCharacteristic->addDescriptor(new BLE2902()); // Mandatory for notifications 
  humidCharacteristic->addDescriptor(new BLE2902());
  pressureCharacteristic->addDescriptor(new BLE2902());

  // Create battery characteristics
  batteryCharacteristic = bService->createCharacteristic(BLEUUID((uint16_t)0x2A19),BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY);
  bService->addCharacteristic(batteryCharacteristic);
  batteryCharacteristic->addDescriptor(new BLE2902());
  
  // Start services
  eService->start();
  bService->start();
  
  // Setup advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(EnvironmentService);
  pAdvertising->addServiceUUID(BatteryService);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);
  BLEDevice::startAdvertising();
}

void loop() {
  
  // Get bme280 data
  temperature = bme.readTemperature();
  pressure = bme.readPressure() / 100.0F;
  humidity = bme.readHumidity();
  if(deviceConnected){
    int ble_t = temperature * 100;
    int ble_h = humidity * 100;
    int ble_p = pressure * 1000;
    int bat = 0;
    
    tempCharacteristic->setValue(ble_t);
    tempCharacteristic->notify();
    pressureCharacteristic->setValue(ble_p);
    pressureCharacteristic->notify();
    humidCharacteristic->setValue(ble_h);
    humidCharacteristic->notify();
    batteryCharacteristic->setValue(bat);
    batteryCharacteristic->notify();
    
  }else{
    BLEDevice::startAdvertising();
  }
  
  Serial.print("Temperature = ");
  Serial.print(temperature);
  Serial.println("*C");

  Serial.print("Pressure = ");
  Serial.print(pressure);
  Serial.println("hPa");

  Serial.print("Humidity = ");
  Serial.print(humidity);
  Serial.println("%");
  delay(1000);
}

